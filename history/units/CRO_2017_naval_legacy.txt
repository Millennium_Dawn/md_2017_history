units = {
    fleet = {
        name = "Navy Flotilla"
        naval_base = 3924 #Split
        task_force = {
            name = "Surface Action Squadron"
			location = 3924
            ship = { name = "Vukovar" definition = corvette start_experience_factor = 0.70 equipment = { corvette_hull_2 = { amount = 1 owner = FIN version_name = "Helsinki Class" } }  }
			ship = { name = "Dubrovnik" definition = corvette start_experience_factor = 0.70 equipment = { corvette_hull_2 = { amount = 1 owner = FIN version_name = "Helsinki Class" } }  }
        }
    }
}